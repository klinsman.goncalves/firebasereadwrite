package br.com.samples.firebasereadwrite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import br.com.samples.firebasereadwrite.adapter.ListImageAdapter;
import br.com.samples.firebasereadwrite.model.PratoVO;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference ref;

    ListView lvPratosPrincipais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvPratosPrincipais = findViewById(R.id.lv_principal);

        database = FirebaseDatabase.getInstance();
        ref = database.getReference("pratos");

        ref.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<PratoVO> pratoVOList = new ArrayList<>();
                try {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        PratoVO prato = snapshot.getValue(PratoVO.class);
                        pratoVOList.add(prato);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "ERROR occurred", Toast.LENGTH_LONG).show();
                }
                showList(pratoVOList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void showList(List<PratoVO> pratos){
        ListImageAdapter<PratoVO> pratosAdapter = new ListImageAdapter<PratoVO>(pratos);
        lvPratosPrincipais.setAdapter(pratosAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_novo){
            Intent intent = new Intent(this, AddPratoActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
