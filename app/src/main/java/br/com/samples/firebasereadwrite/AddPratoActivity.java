package br.com.samples.firebasereadwrite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.samples.firebasereadwrite.model.PratoVO;

public class AddPratoActivity extends AppCompatActivity {


    FirebaseDatabase database;
    DatabaseReference myRef;

    Button btAdd;
    TextInputLayout tivNome;
    TextInputLayout tivAcompanhamento;
    TextInputLayout tivValor;
    TextInputLayout tivImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_prato);

        tivNome = findViewById(R.id.tiv_nome);
        tivAcompanhamento = findViewById(R.id.tiv_acompanhamento);
        tivValor = findViewById(R.id.tiv_valor);
        tivImageUrl = findViewById(R.id.tiv_image_url);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("pratos");

        btAdd = findViewById(R.id.bt_add);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewChild(new PratoVO(tivAcompanhamento.getEditText().getText().toString(),
                        tivNome.getEditText().getText().toString(),
                        Double.valueOf(tivValor.getEditText().getText().toString()),
                        tivImageUrl.getEditText().getText().toString()));
            }
        });

    }

    private void addPrato(PratoVO pratoVO){
        myRef.setValue(pratoVO);
    }

    private void addNewChild(PratoVO pratoVO){
        String key = myRef.push().getKey();
        myRef.child(key).setValue(pratoVO).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Data Added", Toast.LENGTH_LONG).show();
            }
        });
    }


}
