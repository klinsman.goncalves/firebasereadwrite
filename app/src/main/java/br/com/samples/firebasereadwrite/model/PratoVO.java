package br.com.samples.firebasereadwrite.model;

import com.google.firebase.database.Exclude;

public class PratoVO extends ListViewData {
    private String acompanhamento;
    private String nome;
    private Double valor;
    private String imageUrl;

    public PratoVO() {
    }

    public PratoVO(String acompanhamento, String nome, Double valor, String imageUrl) {
        this.acompanhamento = acompanhamento;
        this.nome = nome;
        this.valor = valor;
        this.imageUrl = imageUrl;
    }

    public String getAcompanhamento() {
        return acompanhamento;
    }

    public void setAcompanhamento(String acompanhamento) {
        this.acompanhamento = acompanhamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "PratoVO{" +
                "acompanhamento='" + acompanhamento + '\'' +
                ", nome='" + nome + '\'' +
                ", valor=" + valor +
                '}';
    }

    @Exclude
    @Override
    public String getImageUrlListData() {
        return getImageUrl();
    }

    @Exclude
    @Override
    public String getDescriptionListData() {
        return getNome();
    }
}
