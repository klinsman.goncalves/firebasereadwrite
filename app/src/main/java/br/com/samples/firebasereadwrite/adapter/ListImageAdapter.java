package br.com.samples.firebasereadwrite.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.samples.firebasereadwrite.R;
import br.com.samples.firebasereadwrite.model.ListViewData;


public class ListImageAdapter<T extends ListViewData> extends BaseAdapter  {

    List<T> listItens;

    public ListImageAdapter(List<T> items) {
        this.listItens = items;
    }

    @Override
    public int getCount() {
        return listItens.size();
    }

    @Override
    public T getItem(int position) {
        return listItens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_image_item, parent, false);
        T listItem = listItens.get(position);
        TextView tvRegion = view.findViewById(R.id.tv_item_description);
        ImageView imgRegion = view.findViewById(R.id.iv_item_ico);
        tvRegion.setText(listItem.getDescriptionListData());

        Picasso.with(context)
                .load(listItem.getImageUrlListData())
                .placeholder(R.drawable.progress_animation)
                .into(imgRegion);

        return view;
    }
}
